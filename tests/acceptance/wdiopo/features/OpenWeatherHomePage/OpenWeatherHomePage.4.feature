Feature: Second end-to-end test
    As a user on the Open Weather Map site, I want to Enter Invalid Credentials and verify the result

    Background:
        Given I am on the "OpenWeatherHomePage" page

    @Test4
    Scenario Outline: Login "<UserData>"
        When I consider "<UserData>" from "testDataFile"
        And I "click" "HP_SignInLink" "Link"
        Then I "enter" "EmailTextBox" with "EmailTextBox" value
        Then I "enter" "PasswordTextBox" with "PasswordTextBox" value
        And I "click" "SubmitButton" "button"
        Then I Explicitly wait for "3000" milliseconds
        And I "verify" "Failure" field contains "Failure" value

        Examples: User to to search
            | UserData |
            | User 1   | 


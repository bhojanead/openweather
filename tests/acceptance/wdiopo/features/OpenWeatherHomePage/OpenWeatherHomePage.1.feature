Feature: First end-to-end test
    As a user on the Open Weather Map site, I want to perform a UIBehaviourTest in Homepage

    Background:
        Given I am on the "OpenWeatherHomePage" page

    @Test1
    Scenario: 1 Home Page UI verification
        And I "verify" "Page Header" fields contain "Header Fields" values
        And I "verify" "SearchButton" field contains "Search" value
        And I "verify" "CurrentLocationLink" field contains "Current Location" value
        And I "verify" "CurrnetW&FinYourCityLbl" field contains "Current Weather Forecasts" value
        And I "verify" "Navigation Tabs" fields contain "Tabs" values
        And I "verify" "MoreWeatherInYourCityBtn" field contains "MoreWeatherInYourCityBtn" value        
        And I "verify" "Sentinel 2" fields contain "Sentinel 2 Information" values
        And I "verify" "APIs for Agriculture" fields contain "Agriculture Information" values
        And I "verify" "Weather Maps 2.0" fields contain "Weather Map Information" values
        And I "verify" "Service Items" fields contain "Service Information" values
        And I "verify" "Page Footer" fields contain "Footer Fields" values

  